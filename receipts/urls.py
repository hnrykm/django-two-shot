from django.urls import path
from receipts.views import (
    all_receipts,
    create_receipt,
    show_categories,
    accounts_list,
    create_category,
    create_account,
)

urlpatterns = [
    path("", all_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_categories, name="category_list"),
    path("accounts/", accounts_list, name="accounts_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
